# TP CHATBOT BACKEND

la partie BackEnd du TP ChatBot PANDACOLA

## Technologies utilisées 

* Lumen : version 5.8.0
* PHP   : version 7.2.0
* MYSQL : version 5.7.24

## Configuration et Lancement de l'application

Avant de lancer l'application, il faut créer une base de donnée avec un nom de : chatbot
vous pouvez changer le nom de la base et les données d'utilisateur dans le fichier de configuration  (.env ) 
que j'ai pas ignoré avec .gitignore juste pour tester l'application.

Après exécuter les commandes suivantes  :

```
cd TPChatBotBackEnd

//cette commande va créer les tables de la base de donnés
php artisan migrate

//cette commande va insérer des données exemples pour tester l'application
php artisan db:seed 

//cette commande va lancer un serveur local pour l'application
php -S localhost:8000 -t public

```