<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Conversation extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    public function items()
    {
        return $this->hasMany('App\ConversationItem', 'conversation_id', 'id');
    }
    public function toArray()
    {
        $data = parent::toArray();

        if ($this->items) {
            $data['items'] = $this->items;
        } else {
            $data['items'] = null;
        }

        return $data;
    }
}
