<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class ConversationItem extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'conversation_item';
    protected $fillable = ['conversation_id', 'question_id', 'response_id'];
    public function conversation()
    {
        return $this->belongsTo('App\Conversation', 'id', 'conversation_id');
    }
    public function question()
    {
        return $this->hasOne('App\Question', 'id', 'question_id');
    }
    public function response()
    {
        return $this->hasOne('App\Response', 'id', 'response_id');
    }
    public function toArray()
    {
        $data = parent::toArray();
        if ($this->question) {
            $data['question'] = $this->question->label;
        } else {
            $data['question'] = null;
        }
        if ($this->response) {
            $data['response'] = $this->response->label;
        } else {
            $data['response'] = null;
        }

        return $data;
    }
}
