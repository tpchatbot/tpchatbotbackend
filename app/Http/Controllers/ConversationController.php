<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\ConversationItem;
use Illuminate\Http\Request;

class ConversationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index()
    {
        return Conversation::all();
    }
    public function show($conversation_id)
    {
        return Conversation::findOrFail($conversation_id);
    }
    public function save(Request $request)
    {
        $conversation = new Conversation;
        $conversation->save();
        foreach ($request->all() as $item) {
            $conversationItem = new ConversationItem;
            $conversationItem->question_id = $item["question_id"];
            $conversationItem->response_id = $item["response_id"];
            $conversation->items()->save($conversationItem);
        }
        return $conversation;
    }
}
