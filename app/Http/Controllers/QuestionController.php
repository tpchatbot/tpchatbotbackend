<?php

namespace App\Http\Controllers;

use App\Question;

class QuestionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index()
    {
        return Question::all();
    }
    public function show($question_id)
    {
        return Response::findOrFail($question_id);
    }
    public function getRootQuestion()
    {
        return Question::first();
    }
}
