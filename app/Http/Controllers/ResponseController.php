<?php

namespace App\Http\Controllers;

use App\Response;

class ResponseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index()
    {
        return Response::all();
    }
    public function show($response_id)
    {
        return Response::findOrFail($response_id);
    }
    //
}
