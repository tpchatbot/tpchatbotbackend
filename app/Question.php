<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Question extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    public function responses()
    {
        return $this->hasMany('App\Response', 'parentQuestion_id', 'id');
    }
    public function toArray()
    {
        $data = parent::toArray();

        if ($this->responses) {
            $data['responses'] = $this->responses;
        } else {
            $data['responses'] = null;
        }

        return $data;
    }
}
