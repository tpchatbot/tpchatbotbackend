<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Response extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    public function parentQuestion()
    {
        return $this->belongsTo('App\Question', 'id', 'parentQuestion_id');
    }
    public function childQuestion()
    {
        return $this->hasOne('App\Question', 'id', 'childQuestion_id');
    }
    public function toArray()
    {
        $data = parent::toArray();
        if ($this->childQuestion) {
            $data['childQuestion'] = $this->childQuestion;
        } else {
            $data['childQuestion'] = null;
        }

        return $data;
    }
}
