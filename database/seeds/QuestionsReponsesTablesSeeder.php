<?php

use Illuminate\Database\Seeder;
use App\Question;
use App\Response;

class QuestionsReponsesTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $question1 = new Question();
        $question1->label = "Bonjour! En quoi puis je vous aider?";
        $question1->save();


        $question2 = new Question();
        $question2->label = "Notre module de perso est incroyable pour de nombreuses
        raisons... Je vous invite à télécharger votre logo ​ici​.";
        $question2->save();


        $question3 = new Question();
        $question3->label = "Bien entendu! Notre équipe sera ravie de vous assister par
        téléphone au numéro suivant : 03 90 22 42 22";
        $question3->save();


        $question4 = new Question();
        $question4->label = "Assurément, nous sommes tous ici, basés à
        Strasbourg!";
        $question4->save();

        $response1 = new Response();
        $response1->label = "J’aimerais en savoir plus sur votre module de perso";
        $response1->parentQuestion_id = Question::find(1)->id;
        $response1->childQuestion_id = Question::find(2)->id;
        $response1->save();

        $response2 = new Response();
        $response2->label = "Si je souhaite faire une demande de devis, aurais je un
        interlocuteur pour m’assister?";
        $response2->parentQuestion_id = Question::find(1)->id;
        $response2->childQuestion_id = Question::find(3)->id;
        $response2->save();

        $response3 = new Response();
        $response3->label = "Très bien, votre service d’assistance est il basé en
        France?";
        $response3->parentQuestion_id = Question::find(3)->id;
        $response3->childQuestion_id = Question::find(4)->id;
        $response3->save();
    }
}
